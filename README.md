# rajahan <sub><sup>[ra·jah·an]</sup></sub> _verb_

PHP package for encryption/decryption using AES algorithm compatible with CryptoJS.

### Algorithm

<table>
    <tr>
        <td>Cipher</td>
        <td>AES-256</td>
    </tr>
    <tr>
        <td>Mode</td>
        <td>CBC</td>
    </tr>
    <tr>
        <td>Key Derivation</td>
        <td>evpkdf (OpenSSL custom, MD5, 1 iteration)</td>
    </tr>
</table>

## Installation

Installation is best done via Composer, you may use the following command:

```bash
composer require dentmate/rajahan
```

This will add the latest release of [rajahan](https://gitlab.com/dentmate/rajahan) as a module to your project

## Example

This is an example code for using this library:

```php
<?php

use DentMate\Rajahan\AES;

$passphrase = 'my passphrase';
$plain = 'example value';

$encrypted = AES::encrypt($plain, $passphrase);
print_r('Encrypted : ' . $encrypted);

$decrypted = AES::decrypt($encrypted, $passphrase);
print_r('Decrypted : ' . $decrypted);

```

## License

Code licensed under [Apache 2.0 License](./LICENSE).
