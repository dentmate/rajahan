<?php

namespace DentMate\Rajahan;

final class AES
{
    private static function evpkdf($passphrase, $salt)
    {
        $salted = $dx = '';

        while (strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }

        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);

        return [$key, $iv];
    }

    private static function encode($ct, $salt)
    {
        return base64_encode('Salted__' . $salt . $ct);
    }

    private static function decode($base64)
    {
        $decoded = base64_decode($base64);

        if (substr($decoded, 0, 8) !== 'Salted__') {
            throw new \InvalidArgumentException();
        }

        $salt = substr($decoded, 8, 8);
        $ct = substr($decoded, 16);

        return [$ct, $salt];
    }

    /**
     * @param string $plain
     * @param string $passphrase
     * @param null $salt
     * @return string
     */
    public static function encrypt($plain, $passphrase, $salt = null)
    {
        $salt = $salt ?: openssl_random_pseudo_bytes(8);
        [$key, $iv] = self::evpkdf($passphrase, $salt);

        $ct = openssl_encrypt($plain, 'aes-256-cbc', $key, true, $iv);
        return self::encode($ct, $salt);
    }

    /**
     * @param string $base64
     * @param string $passphrase
     * @return string
     */
    public static function decrypt($base64, $passphrase)
    {
        [$ct, $salt] = self::decode($base64);
        [$key, $iv] = self::evpkdf($passphrase, $salt);

        $plain = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return $plain;
    }
}
